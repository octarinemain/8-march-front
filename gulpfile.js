'use strict';

const gulp = require('gulp');
const path = {
	dist: { 
		html: 'dist/',
		style: 'dist/css/',
		js: 'dist/js/',
		img: 'dist/img/',
		video: 'dist/video/'
	},
	src: {
		html: 'src/*.html',
		style: 'src/scss/**/*.scss',
		js: 'src/js/**/*.js',
		img: 'src/img/**/*',
		video: 'src/video/**/*.mp4'
	},
	watch: {
		html: 'src/*.html',
		style: 'src/scss/**/*.scss',
		js: 'src/js/**/*.js'
	}
};

// Lazy Task
function lazyRequireTask(taskName, path, options) {
	options = options || {};
	options.taskName = taskName;
	gulp.task(taskName, function(callback) {
		let task = require(path).call(this, options);
		return task(callback);
	});
}

// HTML
lazyRequireTask('html', './tasks/html.js', {
	src: path.src.html,
	dist: path.dist.html
});

// SCSS to CSS
lazyRequireTask('style', './tasks/style.js', {
	src: path.src.style,
	dist: path.dist.style
});

// JS
lazyRequireTask('js', './tasks/script.js', {
	src: path.src.js,
	dist: path.dist.js
});

// Img
lazyRequireTask('img', './tasks/img-min.js', {
	src: path.src.img,
	dist: path.dist.img
});

// Videos
lazyRequireTask('video', './tasks/video.js', {
	src: path.src.video,
	dist: path.dist.video
});

// Clear dir
lazyRequireTask('clean', './tasks/clean.js', {
	src: 'dist'
});

// Browser-Sync
lazyRequireTask('browser-sync', './tasks/browser-sync.js', {
	src: 'dist/**/*.*'
});

// Builder
gulp.task('build', gulp.parallel(
	'html',
	'style',
	'js',
	'img',
	'video'
));

// Watcher
lazyRequireTask('watch', './tasks/watcher.js', {
	htmlWatch: path.watch.html,
	styleWatch: path.watch.style,
	jsWatch: path.watch.js
});

// Start
gulp.task('default', gulp.series('build', gulp.parallel(
	'watch',
	'browser-sync'
)));

// gulp --dev (with sourcemaps)
// gulp (without sourcemaps)