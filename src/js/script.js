'use strict';

document.addEventListener('DOMContentLoaded', function() {

	let videoArray = [
		'beauty.mp4',
		'ease.mp4',
		'harmonies.mp4',
		'inspiration.mp4',
		'life.mp4',
		'originality.mp4',
		'perfect.mp4',
		'wisdom.mp4'
	];
	let count = Math.floor(Math.random() * videoArray.length);
	document.getElementsByTagName('source')[0].src = `video/${videoArray[count]}`;
	let video = document.getElementById('bg_video');
	video.load();
	video.play(); 
});