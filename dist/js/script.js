'use strict';

document.addEventListener('DOMContentLoaded', function () {

  var videoArray = [
  'beauty.mp4',
  'ease.mp4',
  'harmonies.mp4',
  'inspiration.mp4',
  'life.mp4',
  'originality.mp4',
  'perfect.mp4',
  'wisdom.mp4'];

  var count = Math.floor(Math.random() * videoArray.length);
  document.getElementsByTagName('source')[0].src = "video/".concat(videoArray[count]);
  var video = document.getElementById('bg_video');
  video.load();
  video.play();
});