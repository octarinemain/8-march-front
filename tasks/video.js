'use strict';

const gulp = require('gulp');
const $ = require('gulp-load-plugins')();

module.exports = function(options) {
	return function() {
		return gulp.src(options.src)
			.pipe($.plumber({
				errorHandler: $.notify.onError(function(err) {
					return {
						title: 'video',
						message: err.message
					};
				})
			}))
			.pipe($.cached('video'))
			.pipe($.debug({title: 'DEBUG video'}))
			.pipe($.remember('video'))
			.pipe(gulp.dest(options.dist));
	};
};